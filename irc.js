$("#wiki-go-start").click(function(){
    $("#wiki-main").hide("slow");
    $("#wiki-start").show("slow");
});

$("#wiki-go-cmd").click(function(){
    $("#wiki-main").hide("slow");
    $("#wiki-cmd").show("slow");
});

$("#wiki-go-friendly").click(function(){
    $("#wiki-main").hide("slow");
    $("#wiki-friendly").show("slow");
});

$(".wiki-back").click(function(){
    $(this).parent().hide("slow");
    $("#wiki-main").show("slow");
});
