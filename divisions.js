var createRankBadge = function(rank) {
    var cl = "";
    if (rank.indexOf("Fleet Admiral") != -1) {
        cl = "badge-diamond";
    } else if (rank.indexOf("Admiral") != -1) {
        cl = "badge-warning";
    } else if (rank.indexOf("Commander") != -1) {
        cl = "badge-bronze";
    } else if (rank.indexOf("Lieutenant") != -1) {
        cl = "badge-inverse";
    } else if (rank.indexOf("Chief") != -1 || rank.indexOf("Petty") !=-1) {
        cl = "label-primary";
    } else {
        cl = "label-default";
    }

    var link = "ranks.html#rank-" + rank.toLowerCase().replace(" ", "-");
    return $("<a href=\"" + link + "\"><span class=\"label " + cl + "\">" + rank + "</span></a>");
};
var createList = function() {
    var resp = jQuery.getJSON("divisions.json");
    // Set content to be fluid
    $("#content").addClass("row");

    resp.done(function(e) {
        // Global variables for the "boxes"
        var li, i;

        var securitybox = $("<div class=\"col-md-6\"></div>");
        $("<h4><a href=\"divisions.html?bmnsd\">Security</a> (" + e.bmnsd.length + ")</h4>").appendTo(securitybox);
        var bmnsd = $("<ul></ul>");
        for (i = 0;i < e.bmnsd.length; i++) {
            li = $("<li></li>");
            createRankBadge(e.bmnsd[i][1]).appendTo(li);
            $("<span> </span><a href=\"members.html?" + encodeURIComponent(e.bmnsd[i][0]) + "\">" + e.bmnsd[i][0] + "</a>").appendTo(li);
            li.appendTo(bmnsd);
        }
        bmnsd.appendTo(securitybox);
        securitybox.appendTo("#content");

        var technicalbox = $("<div class=\"col-md-6\"></div>");
        $("<h4><a href=\"divisions.html?bmntd\">Technical</a> (" + e.bmntd.length + ")</h4>").appendTo(technicalbox);
        var bmntd = $("<ul></ul>");
        for (i = 0;i < e.bmntd.length; i++) {
            li = $("<li></li>");
            createRankBadge(e.bmntd[i][1]).appendTo(li);
            $("<span> </span><a href=\"members.html?" + encodeURIComponent(e.bmntd[i][0]) + "\">" + e.bmntd[i][0] + "</a>").appendTo(li);
            li.appendTo(bmntd);
        }
        bmntd.appendTo(technicalbox);
        technicalbox.appendTo("#content");

        $("<h4><a href=\"divisions.html?bmnprd\">Public Relations</a> (" + e.bmnprd.length + ")</h4>").appendTo(securitybox);
        var bmnprd = $("<ul></ul>");
        for (i = 0;i < e.bmnprd.length; i++) {
            li = $("<li></li>");
            createRankBadge(e.bmnprd[i][1]).appendTo(li);
            $("<span> </span><a href=\"members.html?" + encodeURIComponent(e.bmnprd[i][0]) + "\">" + e.bmnprd[i][0] + "</a>").appendTo(li);
            li.appendTo(bmnprd);
        }
        bmnprd.appendTo(securitybox);

        $("<h4><a href=\"divisions.html?bmnrd\">Records</a> (" + e.bmnrd.length + ")</h4>").appendTo(technicalbox);
        var bmnrd = $("<ul></ul>");
        for (i = 0;i < e.bmnrd.length; i++) {
            li = $("<li></li>");
            createRankBadge(e.bmnrd[i][1]).appendTo(li);
            $("<span> </span><a href=\"members.html?" + encodeURIComponent(e.bmnrd[i][0]) + "\">" + e.bmnrd[i][0] + "</a>").appendTo(li);
            li.appendTo(bmnrd);
        }
        bmnrd.appendTo(technicalbox);


        $("<h4><a href=\"divisions.html?bmnrdd\">Research and Development</a> (" + e.bmnrdd.length + ")</h4></a>").appendTo(securitybox);
        var bmnrdd = $("<ul></ul>");
        for (i = 0;i < e.bmnrdd.length; i++) {
            li = $("<li></li>");
            createRankBadge(e.bmnrdd[i][1]).appendTo(li);
            $("<span> </span><a href=\"members.html?" + encodeURIComponent(e.bmnrdd[i][0]) + "\">" + e.bmnrdd[i][0] + "</a>").appendTo(li);
            li.appendTo(bmnrdd);
        }
        bmnrdd.appendTo(securitybox);
        // Add the date of the last update
        $("<small class=\"text-muted\">Last updated: " + e.updated + "</small>").appendTo(technicalbox);

    });
};

var createPage = function(name) {
    var resp = jQuery.getJSON("divisions/" + name + ".json");
    resp.done(function(e){
        // Title
        if (name == "bmnrd") {
            name = "Records Division";
        }
        else if (name == "bmntd") {
            name = "Technical Division";
        }
        else if (name == "bmnprd") {
            name = "Public Relations Division";
        }
        else if (name == "bmnrdd") {
            name = "Research and Development Division";
        }
        else if (name == "bmnsd") {
            name = "Security Division";
        }
        var title = $("<h4>" + name + " </h4>");
        title.appendTo("#content");
        // Links
        for (var i in e.links) {
            if (i == "Group post") {
                $("<a class=\"text-muted\" href=\"" + e.links[i] + "\">" + i + "</a>").appendTo("#content");
            }
            else {
                $("<span class=\"text-muted\">&nbsp;&middot;&nbsp;</span><a class=\"text-muted\" href=\"" + e.links[i] + "\">" + i + "</a>").appendTo("#content");
            }
        }
        // Aaand add the page
        var ul1, col1, ul2, col2;
        $("<div class=\"member-page\">"+ e.description +"</div>").appendTo("#content");
        col1 = $("<div class=\"col-md-6\" id=\"members\"></div>").appendTo("#content");
        ul1 = $("<ul></ul>").appendTo(col1);
        col2 = $("<div class=\"col-md-6\" id=\"requirements\"></div>").appendTo("#content");
        ul2 = $("<ul></ul>").appendTo(col2);
        for (i = 0; i < e.members.length; i++) {
            $("<li><a href=\"members.html?" + e.members[i] + "\">" + e.members[i] + "</li>").appendTo(ul1);
        }
        for (i = 0;i < e.requirements.length; i++) {
            $("<li>" + e.requirements[i] + "</li>").appendTo(ul2);
        }
        // Prepend the back link
        $("<a href=\"divisions.html\">< Back</a><br>").prependTo("#content");
        $("<h5>Members</h5>").prependTo("#members");
        $("<h5>Requirements</h5>").prependTo("#requirements");
    });

    resp.error(function() {
        // Prepend an error message
        $("<a href=\"divisions.html\">< Back</a>").prependTo("#content");
        $("<div class=\"alert alert-danger\"><strong>Sorry,</strong> I couldn't find a division page for " + name + " :(</div>").appendTo("#content");
    });
};

var load = function() {
    var s = document.location.search.substring(1);
    if (s === "") {
        createList();
    } else {
        createPage(s);
    }
};

$(load);
