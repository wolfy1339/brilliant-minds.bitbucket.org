var resp = jQuery.getJSON("clan.json");
resp.done(function(data){
    $("#lvl").text("Clan Level: "+ data.clan.level);
    $("#win").text("War Wins: "+ data.clan.win);
    $("#type").text("Type: "+ data.clan.type);
    $("#tro").text("Required Trophies: "+ data.clan.trophies);
    $("#war").text("War Frequency:  "+ data.clan.war);
    $("#loc").text("Clan Location:  "+ data.clan.location);
    $("#tag").text("Tag:  "+ data.clan.tag);
    // Here's where you handle a successful response.
    for (var i = 0; i < data.clan.leaders.length; i++) {
        var ul = $("#mods");
        var li = $("<li>" + data.clan.leaders[i][0] + " (" + data.clan.leaders[i][1] + ")</li>");
        li.appendTo(ul);
    }
});
resp.error(function() {
        // Here's where you handle an error response.
        // Note that if the error was due to a CORS issue,
        // this function will still fire, but there won't be any additional
        // information about the error.
        $("<div class=\"alert alert-danger\"><strong>Darnet!</strong> Something went wrong and I couldn't fetch the required JSON to fill in the Clan data.</div>").appendTo("#iferr");
});
